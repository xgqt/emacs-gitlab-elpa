# Emacs GitLab ELPA

<p align="center">
  <a href="https://archive.softwareheritage.org/browse/origin/?origin_url=https://gitlab.com/xgqt/emacs-gitlab-elpa">
    <img src="https://archive.softwareheritage.org/badge/origin/https://gitlab.com/xgqt/emacs-gitlab-elpa/">
  </a>
  <a href="https://gitlab.com/xgqt/emacs-gitlab-elpa/pipelines">
    <img src="https://gitlab.com/xgqt/emacs-gitlab-elpa/badges/master/pipeline.svg">
  </a>
</p>

Make an ELPA on GitLab pages

<img src="logo.png" alt="logo" width="200"/>

## About

### Installation

Add it to `package-archives`:

``` emacs-lisp
(progn
  (require 'package)
  (add-to-list 'package-archives
               '("xgqt" . "https://xgqt.gitlab.io/emacs-gitlab-elpa/packages/"))
  (package-initialize)
  (package-refresh-contents))
```

### Website

The rendered website published through GitLab pages can be viewed at
[xgqt.gitlab.io/emacs-gitlab-elpa](https://xgqt.gitlab.io/emacs-gitlab-elpa/).


## License

    Copyright (c) 2022-2025, Maciej Barć <xgqt@xgqt.org>
    Licensed under the GNU GPL v2 License
    SPDX-License-Identifier: GPL-2.0-or-later
