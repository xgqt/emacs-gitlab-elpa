# Copyright (c) 2022-2025, Maciej Barć <xgqt@xgqt.org>

PWD                      = $(shell pwd)
SCRIPTS                 := $(PWD)/scripts

SH                      ?= sh
MAKE                    ?= make

RM                      := rm -f -r

.PHONY: all
all:
	$(MAKE) clean
	$(MAKE) build

.PHONY: clean
clean:
	$(RM) /tmp/emacs-gitlab-elpa

.PHONY: build
build:
	$(SH) $(SCRIPTS)/build.sh
