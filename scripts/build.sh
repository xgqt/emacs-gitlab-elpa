#!/bin/sh

# This file is part of emacs-gitlab-elpa - make an ELPA on GitLab pages.
# Copyright (c) 2022-2025, Maciej Barć <xgqt@xgqt.org>
# Licensed under the GNU GPL v2 License
# SPDX-License-Identifier: GPL-2.0-or-later
#
# emacs-gitlab-elpa is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# emacs-gitlab-elpa is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with emacs-gitlab-elpa.  If not, see <https://www.gnu.org/licenses/>.

trap "exit 128" INT
set -e
set -u

script_path="${0}"
script_root="$(dirname "${script_path}")"

cd "${script_root}/.."

set -x

emacs -Q --batch --script ./elisp/make-elpa.el
emacs -Q --batch --script ./elisp/make-index.el
