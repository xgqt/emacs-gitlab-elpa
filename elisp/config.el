;;; config --- configuration -*- lexical-binding: t; -*-

;; This file is part of emacs-gitlab-elpa - make an ELPA on GitLab pages.
;; Copyright (c) 2022-2025, Maciej Barć <xgqt@xgqt.org>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later
;;
;; emacs-gitlab-elpa is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.
;;
;; emacs-gitlab-elpa is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with emacs-gitlab-elpa.  If not, see <https://www.gnu.org/licenses/>.

;;; Code:

(defconst temporary-directory
  (expand-file-name "emacs-gitlab-elpa" temporary-file-directory))

(defconst public-directory
  (expand-file-name "public" "."))

(defconst public-packages-directory
  (expand-file-name "packages" public-directory))

(defconst required-package-records
  (read
   (with-temp-buffer
     (insert-file-contents "./packages.eld")
     (buffer-string))))

(defconst package-repo-name-regexp
  "\\(emacs\\|\\w+-elisp\\)-\\(app-\\|lib-\\)*")

(defconst processed-package-records
  (mapcar (lambda (package-record)
            (let* ((package-git-url     (car   package-record))
                   (package-git-tag     (cadr  package-record))
                   (package-repo-source (caddr package-record))
                   ;; For example:
                   ;;   https://gitlab.com/xgqt/emacs-ansilove ---> ansilove
                   (package-name
                    (replace-regexp-in-string package-repo-name-regexp
                                              ""
                                              (file-name-base package-git-url)))
                   (package-full-name   (concat package-name "-" package-git-tag))
                   ;; For example: /tmp/emacs-gitlab-elpa/NAME
                   (package-directory
                    (expand-file-name package-name temporary-directory))
                   ;; For example: /tmp/emacs-gitlab-elpa/NAME/src/lisp
                   (package-real-source
                    (expand-file-name package-repo-source package-directory))
                   ;; For example: /tmp/emacs-gitlab-elpa/NAME-VERSION
                   (package-new-source
                    (expand-file-name package-full-name temporary-directory))
                   ;; For example: /tmp/emacs-gitlab-elpa/NAME-VERSION.tar
                   (package-new-tar
                    (concat package-new-source ".tar")))
              `((url    . ,package-git-url)
                (tag    . ,package-git-tag)
                (source . ,package-repo-source)
                (name   . ,package-name)
                (full   . ,package-full-name)
                (dir    . ,package-directory)
                (src    . ,package-real-source)
                (new    . ,package-new-source)
                (tar    . ,package-new-tar))))
          required-package-records))

(provide 'config)

;;; config.el ends here
