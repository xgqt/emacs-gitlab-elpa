#!/usr/bin/env -S emacs -Q --batch --script

;;; make-elpa --- Make an ELPA archive -*- lexical-binding: t; -*-

;; This file is part of emacs-gitlab-elpa - make an ELPA on GitLab pages.
;; Copyright (c) 2022-2025, Maciej Barć <xgqt@xgqt.org>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later
;;
;; emacs-gitlab-elpa is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.
;;
;; emacs-gitlab-elpa is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with emacs-gitlab-elpa.  If not, see <https://www.gnu.org/licenses/>.

;;; Code:

(require 'package)
(require 'package-x)

(require 'config (expand-file-name "./elisp/config.el"))

(defun make-package (package-record)
  "Create a package from PACKAGE-RECORD."
  (message "[ %s ] Cloning to %s"
           (alist-get 'name package-record)
           (alist-get 'dir package-record))
  (shell-command (format "git clone --depth 1 --branch %s %s %s"
                         (alist-get 'tag package-record)
                         (alist-get 'url package-record)
                         (alist-get 'dir package-record)))
  (message "[ %s ] Copying sources" (alist-get 'name package-record))
  (shell-command (format "cp -r %s %s"
                         (alist-get 'src package-record)
                         (alist-get 'new package-record)))3
  (message "[ %s ] Writing autoloads" (alist-get 'name package-record))
  (package-generate-autoloads (expand-file-name (alist-get 'name package-record)
                                                (alist-get 'new package-record))
                              (alist-get 'new package-record))
  (message "[ %s ] Writing package definition" (alist-get 'name package-record))
  (with-temp-file (expand-file-name (concat (alist-get 'name package-record) "-pkg.el")
                                    (alist-get 'new package-record))
    (princ "; -*- no-byte-compile: t -*-\n"
           (current-buffer))
    (pp `(define-package ,(alist-get 'name package-record)
           ,(alist-get 'tag package-record)
           ,(format "The %s package" (alist-get 'name package-record))
           '((emacs "24.1"))
           :url ,(alist-get 'url package-record))
        (current-buffer)))
  (message "[ %s ] Creating tarball" (alist-get 'name package-record))
  (shell-command (format "tar --exclude=.git          \
                              --exclude-vcs           \
                              --exclude-vcs-ignores   \
                              -cf %s %s               \
                              --group=0               \
                              --numeric-owner         \
                              --owner=0               \
                              --sort=name"
                         (alist-get 'tar package-record)
                         (alist-get 'full package-record)))
  (message "[ %s ] Uploading tarball" (alist-get 'name package-record))
  (package-upload-file (alist-get 'tar package-record)))

(mapc (lambda (d)
        (delete-directory d t)
        (make-directory d t))
      (list temporary-directory
            public-packages-directory))

(setq package-archive-upload-base public-packages-directory)

(let ((default-directory temporary-directory))
  (mapc #'make-package processed-package-records))

(provide 'make-elpa)

;;; make-elpa.el ends here
