#!/usr/bin/env -S emacs -Q --batch --script

;;; make-index --- Make an ELPA index -*- lexical-binding: t; -*-

;; This file is part of emacs-gitlab-elpa - make an ELPA on GitLab pages.
;; Copyright (c) 2022-2025, Maciej Barć <xgqt@xgqt.org>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later
;;
;; emacs-gitlab-elpa is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.
;;
;; emacs-gitlab-elpa is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with emacs-gitlab-elpa.  If not, see <https://www.gnu.org/licenses/>.

;; TODO: Rewrite in Org with export to HTML?

;;; Code:

(require 'config (expand-file-name "./elisp/config.el"))

(defun make-entry (package-record)
  "Create a HTML entry from PACKAGE-RECORD."
  (princ (format "<tr>
        <td>%s</td>
        <td>%s</td>
        <td>%s</td>
      </tr>"
                 (format "<a href=\"%s\">%s</a>"
                         (alist-get 'url package-record)
                         (alist-get 'name package-record))
                 (alist-get 'tag package-record)
                 (format "<a href=\"./packages/%s.tar\">tarball</a>"
                         (alist-get 'full package-record)))
         (current-buffer)))

(with-temp-file "./public/index.html"
  (princ "<!DOCTYPE html>
<html>
  <head>
    <meta charset=\"utf-8\"/>
    <meta name=\"generator\" content=\"GitLab Pages\"/>
    <meta name=\"description\" content=\"ELPA\" />
    <title>ELPA</title>
    <link rel=\"icon\" type=\"image/x-icon\" href=\"favicon.ico\">
    <style>
      body,
      html {
        background-repeat: no-repeat;
        background: linear-gradient(180deg, #18242b, #482a52);
        height: 100%;
        margin: 0;
        margin: auto;
        padding: 0;
        width: 100%;
      }
      a {
        text-decoration: none;
      }
      a:link {
        color: #2aa198;
      }
      a:visited {
        color: #859900;
      }
      td,
      th {
        padding: 1vw;
      }
      #box {
        background-color: #292b2e;
        border-radius: 15px;
        box-shadow: 0 20px 70px #000000;
        color: #aaaaaa;
        font-family: 'andale mono', 'lucida console', monospace;
        font-size: calc(5px + 2vh);
        height: 90%;
        margin: 30px auto;
        width: 80%;
      }
      #logo {
        display: block;
        margin: auto;
        padding: 2vh;
        width: 15vh;
      }
      #table {
        margin: auto;
        padding: 5vh;
      }
    </style>
  </head>
  <body>
    <div id=\"box\">
      <img id=\"logo\" src=\"logo.png\" alt=\"logo\">
      <table id=\"table\">
        <tr>
          <th>Package</th>
          <th>Version</th>
          <th>Download</th>
        </tr>"
         (current-buffer))
  (mapc #'make-entry processed-package-records)
  (princ "</table>
    </div>
  </body>
</html>"
         (current-buffer)))

(when (and (file-exists-p "./favicon.ico")
           (not (file-exists-p "./public/favicon.ico")))
  (copy-file "./favicon.ico" "./public/favicon.ico"))

(when (and (file-exists-p "./logo.png")
           (not (file-exists-p "./public/logo.png")))
  (copy-file "./logo.png" "./public/logo.png"))

(provide 'make-index)

;;; make-index.el ends here
